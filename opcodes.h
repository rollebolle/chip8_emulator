#pragma once

#include "context.h"

void jump(chip8_context* context, uint16_t address);

void call(chip8_context* p_context, uint16_t address);

void skip_if_v_eq(chip8_context* p_context, uint8_t reg, uint8_t value);

void skip_if_v_neq(chip8_context* p_context, uint8_t reg, uint8_t value);

void skip_if_vx_eq_vy(chip8_context* p_context, uint8_t reg1, uint8_t reg2);

void set_v(chip8_context* p_context, uint8_t reg, uint8_t value);

void add_v(chip8_context* p_context, uint8_t reg, uint8_t value);

// Arithmetic ops

void set_vx_to_vy(chip8_context* p_context, uint8_t vx, uint8_t vy);

void set_vx_to_vx_or_vy(chip8_context* p_context, uint8_t vx, uint8_t vy);

void set_vx_to_vx_and_vy(chip8_context* p_context, uint8_t vx, uint8_t vy);

void set_vx_to_vx_xor_vy(chip8_context* p_context, uint8_t vx, uint8_t vy);

void add_vy_to_vx(chip8_context* p_context, uint8_t vx, uint8_t vy);

void sub_vy_from_vx(chip8_context* p_context, uint8_t vx, uint8_t vy);

void shift_vx_right(chip8_context* p_context, uint8_t vx);

void set_vx_to_vy_minus_vx(chip8_context* p_context, uint8_t vx, uint8_t vy);

void shift_vx_left(chip8_context* p_context, uint8_t vx);

// End Arithmetic ops

void skip_if_vx_neq_vy(chip8_context* p_context, uint8_t vx, uint8_t vy);

void set_i_to_adr(chip8_context* p_context, uint16_t address);

void jump_to_addr_plus_v0(chip8_context* p_context, uint16_t address);

void rand_and_value(chip8_context* p_context, uint8_t vx, uint8_t value);

void ret_sub(chip8_context* p_context);

void skip_if_key_state(chip8_context* p_context, uint8_t vx, uint8_t state);