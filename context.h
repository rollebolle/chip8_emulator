#pragma once

#include <stdlib.h>
#include <stdint.h>
#include <Windows.h>

#define MEM_SIZE 4096
#define PROG_START_ADDRESS 0x200
#define DISPLAY_WIDTH 64
#define DISPLAY_HEIGHT 32
#define STACK_SIZE 16
#define REG_SIZE 16
#define FONTSSET_SIZE 80

typedef uint16_t opcode_t;

typedef struct
{	
	uint8_t display[DISPLAY_WIDTH * DISPLAY_HEIGHT];
	uint8_t key[16];
	uint8_t delay_timer;
	uint8_t sound_timer;
	HANDLE* draw_event;
} chip8_hardware;

typedef struct
{	
	uint8_t memory[MEM_SIZE];
	uint8_t V[REG_SIZE];
	uint16_t I;
	uint16_t pc;
	uint16_t stack[STACK_SIZE];
	uint16_t sp;	
	chip8_hardware* p_hw;
} chip8_context;

chip8_context* chip8_context_new();

chip8_hardware* chip8_hardware_new();

extern uint8_t chip8_fontset[];

