#pragma once

#include <stdio.h>
#include "context.h"

#define CHIP8_ERROR(x) {printf("Error: %s\n", x); exit(1);}

#define VALIDATE_ADDRESS(adr) if (adr > MEM_SIZE) CHIP8_ERROR("address out of bounds")

#define VALIDATE_REGISTRY(reg) if (reg > REG_SIZE) CHIP8_ERROR("register number out of bounds")