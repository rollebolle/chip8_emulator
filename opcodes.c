#include "opcodes.h"
#include "error.h"

static void push_stack(chip8_context* p_context, uint16_t value)
{
	if (p_context->sp == STACK_SIZE - 1)
		CHIP8_ERROR("stack overflow");
	
	p_context->stack[p_context->sp++] = value;	
}

void jump(chip8_context* p_context, uint16_t address)
{
	VALIDATE_ADDRESS(address);
	p_context->pc = address;
}

void call(chip8_context* p_context, uint16_t address)
{
	VALIDATE_ADDRESS(address);
	push_stack(p_context, p_context->pc);
	p_context->pc = address;
}

void skip_if_v_eq(chip8_context* p_context, uint8_t reg, uint8_t value)
{
	VALIDATE_REGISTRY(reg);
	if (p_context->V[reg] == value)
		p_context->pc += 2;
}

void skip_if_v_neq(chip8_context* p_context, uint8_t reg, uint8_t value)
{
	VALIDATE_REGISTRY(reg);
	if (p_context->V[reg] != value)
		p_context->pc += 2;
}
void skip_if_vx_eq_vy(chip8_context*p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);

	if (p_context->V[vx] == p_context->V[vy])
		p_context->pc += 2;
}

void set_v(chip8_context* p_context, uint8_t reg, uint8_t value)
{
	VALIDATE_REGISTRY(reg);
	p_context->V[reg] = value;
}

void add_v(chip8_context* p_context, uint8_t reg, uint8_t value)
{
	VALIDATE_REGISTRY(reg);
	p_context->V[reg] += value;
}

void set_vx_to_vy(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[vx] = p_context->V[vy];
}

void set_vx_to_vx_or_vy(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[vx] = p_context->V[vx] | p_context->V[vy];
}

void set_vx_to_vx_and_vy(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[vx] = p_context->V[vx] & p_context->V[vy];
}

void set_vx_to_vx_xor_vy(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[vx] = p_context->V[vx] ^ p_context->V[vy];
}

void add_vy_to_vx(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[0x0F] = p_context->V[vx] + p_context->V[vy] > 0xFF; // carry
	p_context->V[vx] += p_context->V[vy];
}

void sub_vy_from_vx(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[0x0F] = p_context->V[vx] > p_context->V[vy]; // borrow
	p_context->V[vx] -= p_context->V[vy];
}

void shift_vx_right(chip8_context* p_context, uint8_t vx)
{
	VALIDATE_REGISTRY(vx);
	p_context->V[0x0F] = p_context->V[vx] << 7 >> 7; // save LSB
	p_context->V[vx] = p_context->V[vx] >> 1;
}

void set_vx_to_vy_minus_vx(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	p_context->V[0x0F] = p_context->V[vx] < p_context->V[vy]; // borrow
	p_context->V[vx] = p_context->V[vy] - p_context->V[vx];
}


void shift_vx_left(chip8_context* p_context, uint8_t vx)
{
	VALIDATE_REGISTRY(vx);
	p_context->V[0x0F] = p_context->V[vx] >> 7 << 7; // save NSB
	p_context->V[vx] = p_context->V[vx] << 1;
}

void skip_if_vx_neq_vy(chip8_context* p_context, uint8_t vx, uint8_t vy)
{
	VALIDATE_REGISTRY(vx);
	VALIDATE_REGISTRY(vy);
	if (p_context->V[vx] != p_context->V[vx])
		p_context->pc += 2;
}

void set_i_to_adr(chip8_context* p_context, uint16_t address)
{
	VALIDATE_ADDRESS(address);
	p_context->I = address;
}

void jump_to_addr_plus_v0(chip8_context* p_context, uint16_t address)
{
	VALIDATE_ADDRESS(address);
	p_context->pc = address + p_context->V[0];
}

void rand_and_value(chip8_context* p_context, uint8_t vx, uint8_t value)
{
	VALIDATE_REGISTRY(vx);
	p_context->V[vx] = rand(); // will be overflowed into a byte
	p_context->V[vx] = p_context->V[vx] & value;
}

void ret_sub(chip8_context* p_context)
{
	if (p_context->sp == 0)
		CHIP8_ERROR("stack underflow");

	--p_context->sp;
	p_context->pc = p_context->stack[p_context->sp];	
}

void skip_if_key_state(chip8_context* p_context, uint8_t vx, uint8_t state)
{
	VALIDATE_REGISTRY(vx);
	if (p_context->p_hw->key[p_context->V[vx]] == state)
		p_context->pc += 2;
}

