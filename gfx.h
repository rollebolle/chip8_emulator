#pragma once

#include "context.h"

void gfx_setup_gfx(int argc, char* argv[]);

void gfx_run_render_pass(chip8_hardware* p_hw);

void gfx_run_render_thread_async(chip8_hardware* p_hw);

void gfx_update_keys(chip8_hardware* p_hw);

void gfx_clear_display();