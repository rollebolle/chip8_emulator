#include <windows.h>

#include "gfx.h"
#include "error.h"

#define GFX_WIDTH 64
#define GFX_HEIGHT 32

#define SDLDRAW

#ifdef SDLDRAW

#include "SDL.h"

SDL_Window* sdl_window;
SDL_Renderer* sdl_renderer;
int frame = 0;

void gfx_setup_gfx(int argc, char* argv[])
{			
	if (SDL_Init(SDL_INIT_EVERYTHING) == -1)			
		CHIP8_ERROR("failed initing SDL");
	
	sdl_window = SDL_CreateWindow("Chip-8 Emulator", 100, 100, GFX_WIDTH*10, GFX_HEIGHT*10, SDL_WINDOW_SHOWN);
	if (sdl_window == 0)
		CHIP8_ERROR("failed initing SDL");
	
	sdl_renderer = SDL_CreateRenderer(sdl_window, -1, SDL_RENDERER_SOFTWARE);
	if (sdl_renderer == 0)
		CHIP8_ERROR("failed initing SDL");	

	SDL_SetRenderDrawColor(sdl_renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(sdl_renderer);
}

static void set_unset_keys(chip8_hardware* p_hw, SDL_Keycode sym, uint8_t set)
{	
	switch (sym)
	{
	case 122:
		p_hw->key[0] = set;
		break;
	case 120:
		p_hw->key[1] = set;
		break;
	case 99:
		p_hw->key[2] = set;
		break;
	case 118:
		p_hw->key[3] = set;
		break;

	case 97:
		p_hw->key[4] = set;
		break;
	case 115:
		p_hw->key[5] = set;
		break;
	case 100:
		p_hw->key[6] = set;
		break;
	case 102:
		p_hw->key[7] = set;
		break;

	case 113:
		p_hw->key[8] = set;
		break;
	case 119:
		p_hw->key[9] = set;
		break;
	case 101:
		p_hw->key[10] = set;
		break;
	case 114:
		p_hw->key[11] = set;
		break;

	case 49:
		p_hw->key[12] = set;
		break;
	case 50:
		p_hw->key[13] = set;
		break;
	case 51:
		p_hw->key[14] = set;
		break;
	case 52:
		p_hw->key[15 ] = set;
		break;

	default:
		break;
	}
}

void gfx_update_keys(chip8_hardware* p_hw)
{
	SDL_Event event;
	while( SDL_PollEvent( &event ) )
	{
		//If the user has Xed out the window
		switch (event.type)
		{
		case SDL_KEYDOWN:
			set_unset_keys(p_hw, event.key.keysym.sym, 1);
			break;			
		case SDL_KEYUP:
			set_unset_keys(p_hw, event.key.keysym.sym, 0);
			break;
		case SDL_QUIT:			
			SDL_DestroyRenderer(sdl_renderer);
			SDL_DestroyWindow(sdl_window);
			SDL_Quit();
			exit(1);
			break;
		default:
			break;
		}
	}
}

void gfx_run_render_pass(chip8_hardware* p_hw)
{	
	uint16_t i = 0;
	uint8_t stuff_drawn = 0;	
	SDL_Rect rect = {20, 20, 20, 20};	

	gfx_update_keys(p_hw);

	// wait for draw event to avoid flicker
	WaitForSingleObject(*p_hw->draw_event, INFINITE);	
	
	SDL_SetRenderDrawColor(sdl_renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(sdl_renderer);
		
	for (i = 0; i < DISPLAY_WIDTH * DISPLAY_HEIGHT; i++)
	{
		SDL_Rect rect = {(i - DISPLAY_WIDTH * (i / DISPLAY_WIDTH))*10, (i / DISPLAY_WIDTH)*10, 10, 10};

		if (p_hw->display[i] == 0)
		{
			continue;
		}

		SDL_SetRenderDrawColor(sdl_renderer, 0xFF, 0xFF, 0xFF, 0xFF);
		SDL_RenderFillRect(sdl_renderer, &rect);
		stuff_drawn = 1;
	}

	SDL_RenderPresent(sdl_renderer);
	++frame;	
}

void gfx_clear_display()
{
	SDL_SetRenderDrawColor(sdl_renderer, 0x00, 0x00, 0x00, 0xFF);
	SDL_RenderClear(sdl_renderer );
}

static DWORD WINAPI render_thread(LPVOID param)
{
	chip8_hardware* p_hw = (chip8_hardware*) param;
	while (1)
	{
		gfx_run_render_pass(p_hw);

		if (p_hw->delay_timer > 0)
			--p_hw->delay_timer;

		if (p_hw->sound_timer > 0)
			--p_hw->sound_timer;
	}
}

void gfx_run_render_thread_async(chip8_hardware* p_hw)
{	
	HANDLE thread = CreateThread(0,0, render_thread, p_hw, 0, 0);
	if (thread == 0)
		CHIP8_ERROR("error creating thread");
}

#endif // SDLDRAW