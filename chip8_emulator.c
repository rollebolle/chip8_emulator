#include <stdio.h>
#include <string.h>
#include <stdint.h>
#include <stdlib.h>
#include <Windows.h>

#include "error.h"
#include "context.h"
#include "opcodes.h"
#include "gfx.h"

void load_program(chip8_context* p_context, const char* rom)
{
	// TODO load program
	FILE* file = 0;
	uint32_t fsize = 0;
	uint32_t result = 0;
	p_context->pc = PROG_START_ADDRESS;
	file = fopen(rom, "rb");

	if (file == 0)
		CHIP8_ERROR("error opening file");

	fseek(file, 0, SEEK_END);
	fsize = ftell(file);
	fseek(file, 0, SEEK_SET);
	result = fread(&p_context->memory[p_context->pc], 1, fsize, file);
	if (result != fsize)
		CHIP8_ERROR("error reading file");

	fclose(file);

}

opcode_t fetch_instruction(chip8_context* p_context)
{
	opcode_t opcode = 0;	
	opcode = p_context->memory[p_context->pc++] << 8;	
	opcode |= p_context->memory[p_context->pc++];
	return opcode;
}

void decode_and_execute_arithmetic(chip8_context* p_context, opcode_t opcode)
{
	uint8_t vx = (opcode & 0x0F00) >> 8;
	uint8_t vy = (opcode & 0x00F0) >> 4;

	switch (opcode & 0x000F)
	{
	case 0x0000:
		set_vx_to_vy(p_context, vx, vy);
		break;
	case 0x0001:
		set_vx_to_vx_or_vy(p_context, vx, vy);
		break;
	case 0x0002:
		set_vx_to_vx_and_vy(p_context, vx, vy);
		break;
	case 0x0003:
		set_vx_to_vx_xor_vy(p_context, vx, vy);
		break;
	case 0x0004:
		add_vy_to_vx(p_context, vx, vy);
		break;
	case 0x0005:
		sub_vy_from_vx(p_context, vx, vy);
		break;
	case 0x0006:
		shift_vx_right(p_context, vx);
		break;
	case 0x0007:
		set_vx_to_vy_minus_vx(p_context, vx, vy);		
		break;
	case 0x000E:
		shift_vx_left(p_context, vx);
		break;
	default:
		CHIP8_ERROR("decoding error")
	}
}
void decode_and_execute_memory_instructions(chip8_context* p_context, opcode_t opcode)
{
	uint8_t i = 0;
	uint8_t temp;
	uint8_t vx = (opcode & 0x0F00) >> 8;
	VALIDATE_REGISTRY(vx);

	switch (opcode & 0x00FF)
	{
	case 0x0007:
		p_context->V[vx] = p_context->p_hw->delay_timer;
		break;
	case 0x000A:
		// TODO
		break;	
	case 0x0015:
		p_context->p_hw->delay_timer = p_context->V[vx];
		break;
	case 0x0018:
		p_context->p_hw->sound_timer = p_context->V[vx];
		break;
	case 0x001E:
		p_context->I += p_context->V[vx];
		break;
	case 0x0029:		
		p_context->I = p_context->V[vx]*5;
		break;
	case 0x0033:
		temp = p_context->V[vx] / 100;
		p_context->memory[p_context->I] =  temp; // 100 digit
		temp = (p_context->V[vx] % 100) / 10;
		p_context->memory[p_context->I + 1] =  temp; // 10 digit
		p_context->memory[p_context->I + 2] =  temp % 10; // 1 digit
		break;
	case 0x0055:
		for (i = 0; i <= vx; ++i)
		{
			p_context->memory[p_context->I + i] = p_context->V[i];
		}
		break;
	case 0x0065:
		for (i = 0; i <= vx; ++i)
		{
			p_context->V[i] = p_context->memory[p_context->I + i];
		}
		break;
	}
}


void cpy_pixels_to_fb(chip8_context* p_context, uint8_t x_reg, uint8_t y_reg, uint8_t height)
{	
	uint8_t x, y, pixel, yline, xline;

	VALIDATE_REGISTRY(x_reg);
	VALIDATE_REGISTRY(y_reg);

	x = p_context->V[x_reg];
	y = p_context->V[y_reg];
	

	p_context->V[0xF] = 0;
	for (yline = 0; yline < height; yline++)
	{
		pixel = p_context->memory[p_context->I + yline];
		for(xline = 0; xline < 8; xline++)
		{
			if((pixel & (0x80 >> xline)) != 0)
			{
				if(p_context->p_hw->display[(x + xline + ((y + yline) * 64))] == 1)
					p_context->V[0xF] = 1;                                 
				p_context->p_hw->display[x + xline + ((y + yline) * 64)] ^= 1;
			}
		}
	}
}

void decode_and_execute_instruction(chip8_context* p_context, opcode_t opcode)
{
	switch (opcode & 0xF000)
	{
	case 0x0000:
		if (opcode == 0x00E0)
			memset(p_context->p_hw->display, 0, DISPLAY_WIDTH * DISPLAY_HEIGHT);
		else if (opcode == 0x00EE)
			ret_sub(p_context);
		break;
	case 0x1000:
		jump(p_context, opcode & 0x0FFF);
		break;
	case 0x2000:
		call(p_context, opcode & 0x0FFF);
		break;
	case 0x3000:
		skip_if_v_eq(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00FF);
		break;
	case 0x4000:
		skip_if_v_neq(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00FF);
		break;
	case 0x5000:
		skip_if_vx_eq_vy(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00F0);
		break;
	case 0x6000:
		set_v(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00FF);
		break;
	case 0x7000:
		add_v(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00FF);
		break;
	case 0x8000:
		decode_and_execute_arithmetic(p_context, opcode);
		break;
	case 0x9000:
		skip_if_vx_neq_vy(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00F0 >> 4);
		break;
	case 0xA000:
		set_i_to_adr(p_context, opcode & 0x0FFF);
		break;
	case 0xB000:
		jump_to_addr_plus_v0(p_context, opcode & 0x0FFF);
		break;
	case 0xC000:
		rand_and_value(p_context, (opcode & 0x0F00) >> 8, opcode & 0x00FF);
		break;
	case 0xD000:
		// transfer bytes to framebuffer
		cpy_pixels_to_fb(p_context, (opcode & 0x0F00) >> 8, (opcode & 0x00F0) >> 4, opcode & 0x000F);
		if (p_context->V[0x0F] == 0)
			SetEvent(*p_context->p_hw->draw_event);
		break;
	case 0xE000:
		if ((opcode & 0x00FF) == 0x009E)
			skip_if_key_state(p_context, (opcode & 0x0F00) >> 8, 1);
		else
			skip_if_key_state(p_context, (opcode & 0x0F00) >> 8, 0);
		break;
	case 0xF000:
		decode_and_execute_memory_instructions(p_context, opcode);
		break;
	default:
		CHIP8_ERROR("decoding error")
		break;
	}
}

static DWORD WINAPI run_interpreter(LPVOID param)
{
	chip8_context* p_context = (chip8_context*) param;
	int frame = 0;
	while (1)
	{
		opcode_t opcode = fetch_instruction(p_context);
		printf("opcode: 0x%x\n", opcode);
		decode_and_execute_instruction(p_context, opcode);
		Sleep(2);

		if (frame++ % 8 == 0) // ca 60 hz
		{
			if (p_context->p_hw->delay_timer > 0)
				--p_context->p_hw->delay_timer;

			if (p_context->p_hw->sound_timer > 0)
				--p_context->p_hw->sound_timer;
		}		
	}
}

int main(int argc, char* argv[])
{
	uint16_t i = 0;
	uint16_t opcode = 0xFFFF;
	chip8_context* p_context = chip8_context_new();	
	HANDLE draw_event = CreateEvent(0, FALSE, FALSE, 0);
	
	p_context->p_hw->draw_event = &draw_event;
	
	if (argc == 2)
		load_program(p_context, argv[1]);
	else
		load_program(p_context, "PONG");

	// copy fontset
	memcpy(&p_context->memory[0], chip8_fontset, FONTSSET_SIZE);
	
	gfx_setup_gfx(argc, argv);
	CreateThread(0, 0, run_interpreter, p_context, 0, 0);
					
	while (1)
	{					
		gfx_run_render_pass(p_context->p_hw);								
	}
	
	return 0;
}

